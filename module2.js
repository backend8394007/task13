//#1
/*
import {printDate, sum, name} from './module3.js';

printDate();
console.log(sum(2, 2));
console.log(name);
 */

//#2
/*
import * as mod3 from './module3.js';

mod3.printDate();
console.log(mod3.sum(2, 2));
console.log(mod3.name);
 */

//#3
/*
import sayHello from "./module3.js";
sayHello();
 */

//#4
/*
import {printDate as takeDate} from "./module3.js";
takeDate();
 */

//#5
/*
export {printDate, sum, name} from './module3.js';
 */